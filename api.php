<?php
require_once('./config.php');
require_once('./DatabaseHelper.php');

header('Content-Type: application/json');

if ( isset($_GET['action']) ) {
    $_POST['action'] = $_GET['action'];
}

if ( !isset($_POST['action']) ) {
    $_POST['action'] = null;
}

try {
    switch ( $_POST['action'] ) {
        case 'reservation': // 第十七屆 - 預定飯店
            $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);

            $check = current( $db->sql_q(
                "SELECT *
                 FROM hotel
                 WHERE confirmed > 0 AND id_number = '".$_POST['id_number']."'"
                ));

            if ( 0 != count($check) && $check != false ) {
                echo json_encode([
                    'status' => 50,
                    'msg'    => '您已預訂過飯店，如需加訂請來信到 wuhsinte@gms.npu.edu.tw',
                ]);
                exit;
            }

            $paramList = [
                'name',
                'id_number',
                'company',
                'job_title',
                'phone',
                'email',
                'receipt_title',
                'EIN',
                'hotel',
                'checkIn',
                'hotel_room',
                'signup'
            ];

            $room = ['A', 'B', 'C', 'D', 'E'];
            for ($i=0; $i < 5; $i++) {
                if ( !empty($_POST['room'.$room[$i]] ) ) {
                    array_push($paramList, 'room'.$room[$i]);
                }
            }
            for ($i=1; $i <= 2; $i++) {
                if ( !empty($_POST['remark'.$i] ) ) {
                    array_push($paramList, 'remark'.$i);
                }
            }

            $fetchedParams = array_merge(['id' => null], fetchParams($paramList));

            if ( $db->insert('hotel', $fetchedParams) ) {
                $fetchedParams['id'] = $db->lastInsertId();
                send_email($fetchedParams['email'], 'ITAOI 2018 訂房通知', genMailContent_reservation($fetchedParams) );
                echo json_encode([
                    'status' => 0,
                    // 'sn'     => $fetchedParams['id'],
                    'msg'    => 'Success.',
                ]);
            } else {
                echo json_encode([
                    'status' => 10,
                    'msg'    => '未知錯誤，請聯絡系統管理員。',
                ]);
                exit;
            }
            break;

        case 'signup':
            $paramList = [
                'name',
                'gender',
                'id_number',
                'birthday',
                'company',
                'signup_food',
                'signup_activity',
                'signup_activity_1',
                'signup_activity_2',
                // 'food_1',
                // 'food_2',
                'job_title',
                'phone',
                'email',
                'receipt_title',
                'eating_habits',
                'reg_paper',
                'paper_id',
                'paper_title',
                // 'identity',
                // 'is_most_plan',
                // 'most_plan_id',
                // 'most_plan_name',
                // 'most_plan_host',
                // 'most_plan_su',
                'comment',
                'password'
            ];

            $fetchedParams = array_merge(['id' => null], fetchParams($paramList));

            // 不註冊論文，將相關欄位標註無值
            if ( '否' === $fetchedParams['reg_paper'] ) {
                $fetchedParams['paper_id']       = '-';
                $fetchedParams['paper_title']    = '-';
                // $fetchedParams['identity']       = '-';
                // $fetchedParams['is_most_plan']   = '-';
            }
            // 非科技部計畫，將相關欄位標註無值
            // if ( in_array($fetchedParams['is_most_plan'], ['否', '-']) ) {
            //     $fetchedParams['most_plan_id']   = '-';
            //     $fetchedParams['most_plan_name'] = '-';
            //     $fetchedParams['most_plan_host'] = '-';
            //     $fetchedParams['most_plan_su']   = '-';
            // }

            $fetchedParams['member_data'] = fetchMembersData();

            $crc32  = sprintf('%u', crc32($fetchedParams['email']));
            $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
            /* 2017-03-20: 大會要求取消檢查
            $count = current( $db->select('signup', ['COUNT(*) AS `count`'], [
                'crc32' => $crc32,
                'email' => $fetchedParams['email']
            ]) )->count;
            if ( 0 < $count ) {
                echo json_encode([
                    'status' => 20,
                    'msg'    => '此Email已完成報名手續，請至您的Email檢查是否有收到報名資料副本，或聯絡大會確認報名結果，謝謝。',
                ]);
                exit;
            }
            */

            $fetchedParams['crc32']       = $crc32;
            $fetchedParams['fee_total']   = caculateFee($fetchedParams);
            $fetchedParams['member_data'] = json_encode($fetchedParams['member_data']);

            if ( $db->insert('signup', $fetchedParams) ) {
                $fetchedParams['id'] = $db->lastInsertId();
                send_email($fetchedParams['email'], 'ITAOI 2018 報名資料副本與繳費通知', genMailContent_signup($fetchedParams) );
                echo json_encode([
                    'status' => 0,
                    'sn'     => $fetchedParams['id'],
                    'msg'    => 'Success.',
                ]);
            } else {
                echo json_encode([
                    'status' => 10,
                    'msg'    => '未知錯誤，請聯絡系統管理員。',
                ]);
                exit;
            }
            break;

        case 'update-payment-recode':
            $paramList = [
                'id',
                'password',
                'payment_account',
                'payment_fee',
                'payment_time',
                'payment_hour',
                'payment_minute',
            ];
            $fetchedParams = (object)fetchParams($paramList);
            $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
            $record = current( $db->select('signup', [], [
                'id'       => $fetchedParams->id,
                'password' => $fetchedParams->password,
            ]) );
            if ( false === $record ) {
                echo json_encode([
                    'status' => 30,
                    'msg'    => '輸入的報名序號或認證碼有誤，請重新輸入。',
                ]);
                exit;
            }
            if ( !empty($record->payment_account) || !empty($record->payment_fee) || !empty($record->payment_time) ) {
                echo json_encode([
                    'status' => 31,
                    'msg'    => '您已填寫過繳費記錄，如欲重新填寫，請連絡大會取得協助，謝謝。',
                ]);
                exit;
            }

            $fetchedParams->payment_time .= " {$fetchedParams->payment_hour}:{$fetchedParams->payment_minute}";
            $updateColumn = [
                'payment_account' => $fetchedParams->payment_account,
                'payment_fee'     => $fetchedParams->payment_fee,
                'payment_time'    => $fetchedParams->payment_time,
            ];

            if ( $db->update('signup', $updateColumn, ['id' => $fetchedParams->id]) ) {
                $mailParams = [
                    $fetchedParams->id,
                    $fetchedParams->payment_account,
                    $fetchedParams->payment_fee,
                    $fetchedParams->payment_time,
                ];
                send_email($record->email, 'ITAOI 2018 大會已收到您的繳費記錄', genMailContent_paymentRecord($mailParams) );
                echo json_encode([
                    'status' => 0,
                    'msg'    => 'Success.',
                ]);
            } else {
                echo json_encode([
                    'status' => 10,
                    'msg'    => '未知錯誤，請聯絡系統管理員。',
                ]);
                exit;
            }
            break;

        case 'get-signup-record':
            $paramList = [
                'id',
                'password',
            ];
            $fetchedParams = (object)fetchParams($paramList);
            $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
            $record = current( $db->select('signup', [], [
                'id'       => $fetchedParams->id,
                'password' => $fetchedParams->password,
            ]) );
            if ( false === $record ) {
                echo json_encode([
                    'status' => 30,
                    'msg'    => '輸入的報名序號或認證碼有誤，請重新輸入。',
                ]);
                exit;
            }

            echo json_encode([
                'status' => 0,
                'msg'    => 'Success.',
                'data'   => genSingupRecordTableHtml( (array)$record )
            ]);
            break;

        case 're-send-mail-signup-record':
            $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
            $record = $db->select('signup', [], ['id' => $_GET['id']]);
            if ( 0 === count($record) ) {
                echo json_encode([
                    'status' => 20,
                    'msg'    => 'Record not found.',
                ]);
                exit;
            }
            $record = current($record);
            unset(
                $record->crc32,
                $record->confirmed,
                $record->payment_account,
                $record->payment_fee,
                $record->payment_time
            );
            echo json_encode([
                'status' => 0,
                'msg'    => 'Success.',
                'result' => send_email($record->email, 'ITAOI 2018 報名資料更新', genMailContent_signup( (array)$record ) )
            ]);
            break;

        case 'get-room': // 第十七屆 - 增加訂房功能
            $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
            $record = current( $db->sql_q(
                'SELECT SUM(roomA) as roomA,
                        SUM(roomB) as roomB,
                        SUM(roomC) as roomC,
                        SUM(roomD) as roomD,
                        SUM(roomE) as roomE
                 FROM hotel
                 WHERE confirmed > 0'
                ));
            if ( 0 === count($record) ) {
                echo json_encode([
                    'status' => 20,
                    'msg'    => 'Record not found.',
                ]);
                exit;
            }
            echo json_encode([
                'status'   => 0,
                'msg'      => 'Success.',
                'response' => $record
            ]);
            break;

        default:
            echo json_encode([
                'status' => 1,
                'msg'    => '不正確的連線請求，請聯絡系統管理員。',
            ]);
            exit;
            break;
    }
} catch (Exception $e) {
    echo json_encode([
        'status' => $e->getCode(),
        'msg'    => "錯誤：\n" . $e->getMessage(),
    ]);
}


function fetchParams( $paramList ) {
    global $_POST;

    // PHP 7.0
    // $fetched = array_filter($_POST, function($value, $key) use ($paramList){
    //     print_r([$value, $key]);
    //     return in_array($key, $paramList);
    // }, ARRAY_FILTER_USE_BOTH );
    $fetched = [];
    if ($_POST['action'] === 'reservation') {
        foreach ($_POST as $key => $value) {
            if ( in_array($key, $paramList) ) {
                switch ($key) {
                  case 'hotel':
                    $fetched['hotel'] = $_POST['hotel'][0];
                    break;

                  case 'checkIn':
                    $fetched['checkIn'] = implode(',', $_POST['checkIn']);
                    break;

                  case 'hotel_room':
                    $fetched['hotel_room'] = $_POST['hotel_room'][0];
                    break;

                  default:
                    $fetched[$key] = $value;
                    break;
                }
            }
        }
    } else {
        foreach ($_POST as $key => $value) {
            if ( in_array($key, $paramList) ) {
                $fetched[$key] = $value;
            }
        }
        //2017-09-24 當不註冊論文,出現資料‘identity’遺漏,以下為檢查foreach跑完是否漏掉‘identity’
        // if ( !in_array('identity', $fetched) && $_POST['action'] === 'signup') {
        //     $fetched['identity'] = '';
        // }
    }

    if ( count($fetched) != count($paramList) ) {
        // print_r($_POST); die;
        throw new Exception("表單資料傳送失敗，請重新整理頁面後再填寫，或使用其他瀏覽器進行報名。", 20001);
        return false;
    }

    return $fetched;
}

function fetchMembersData() {
    global $_POST;
    $paramList = [
        'member_name',
        'member_gender',
        'member_id_number',
        'member_birthday',
        'member_identity_type',
        'member_phone',
        'member_email',
        'member_eating_habits',
        'member_food_1',
        'member_food_2',
        'member_food_3',
        'member_signup_activity',
        'member_signup_activity_1',
        'member_signup_activity_2',
    ];
    $flag = [];
    foreach ($paramList as $value) {
        $flag[] = ( isset($_POST[$value]) && is_array($_POST[$value]) ? count($_POST[$value]) : 0 );
    }
    if ( 1 !== count(array_unique($flag)) ) {
        if ( $flag[11] != $flag[12] || $flag[11] != $flag[13]) {
            # code...
        } else {
            throw new Exception("表單資料傳送失敗，請重新整理頁面後再填寫，或使用其他瀏覽器進行報名。", 20002);
            return false;
        }
    }

    // 沒有隨行人員
    if ( 0 === $flag[0] ) {
        return [];
    }

    // PHP 7.0
    // $fetched = array_filter($_POST, function($value, $key) use ($paramList){
    //     return in_array($key, $paramList);
    // }, ARRAY_FILTER_USE_BOTH);
    $fetched = [];
    foreach ($_POST as $key => $value) {
        if ( in_array($key, $paramList) ) {
            $fetched[$key] = $value;
        }
    }
    $fetched = array_map('array_values', $fetched); // Renew index
    $profile = [];

    $i = 0;
    while ( $i < $flag[0] ) {
        $profile[] = array_combine( $paramList, array_column($fetched, $i) );
        $i++;
    }
    return $profile;
}

// function caculateFee_hotel( array $signupData ) {
//     $priceList = [
//         'A' => 4400, // (喜來登) 街景雙床房
//         'B' => 4800, // (喜來登) 市景雙床房
//         'C' => 5400, // (喜來登) 港景雙床房
//         'D' => 6400, // (喜來登) 港景一大床房
//         'E' => 3950, // (昇恆昌) 園景探索客房2人1室
//     ];

//     $room = ['A', 'B', 'C', 'D', 'E'];

//     $totalFee = 0;
//     for ($i=0; $i < 5; $i++) {
//         if (!empty($signupData['room'.$room[$i]]) && $signupData['room'.$room[$i]] != 0) {
//             $in  = intval(substr($signupData['checkIn'.$room[$i]], 9, 2));
//             $out = intval(substr($signupData['checkOut'.$room[$i]], 9, 2));
//             for ($j=$in-22; $j != $out-22 ; $j++) {
//                 if ( ($j+22 == 26 || $j+22 == 27) && $room[$i] != 'E' ) {
//                     $totalFee += $priceList[$room[$i]] * $signupData['room'.$room[$i]] + ($signupData['room'.$room[$i]] == 0 ? 0 : 500 );
//                     print_r($priceList[$room[$i]]); die;
//                 } else {
//                     $totalFee += $priceList[$room[$i]] * $signupData['room'.$room[$i]];
//                 }
//             }
//         }
//     }

//     return $totalFee;
// }

function caculateFee( array $signupData ) {
    $priceList = [
        'signup_activity' => 1900, // 七美行程
        'member_food_1'   => 100,  // 5/25午餐
        'member_food_2'   => 800,  // 5/25晚宴
        'member_food_3'   => 100,  // 5/27午餐
    ];

    $totalFee = 2300; // 基本報名費
    $totalFee -= ( ( 'ITAOI2018-RS-999' === $signupData['paper_id'] && 'Discount' === $signupData['paper_title'] ) ? 2300 : 0 );
    $totalFee += ( '參加' === $signupData['signup_activity'] ? $priceList['signup_activity'] : 0 );
    $totalFee += ( '是' === $signupData['signup_food'] ? $priceList['member_food_3'] : 0 );
    if ( count($signupData['member_data']) ) {
        foreach ($signupData['member_data'] as $value) {
            if ( '滿3歲' === $value['member_identity_type'] ) {
                $totalFee += ( '參加' === $value['member_signup_activity'] ? $priceList['signup_activity'] : 0 );
                $totalFee += ( '是'   === $value['member_food_1'] ? $priceList['member_food_1'] : 0 );
                $totalFee += ( '是'   === $value['member_food_2'] ? $priceList['member_food_2'] : 0 );
                $totalFee += ( '是'   === $value['member_food_3'] ? $priceList['member_food_3'] : 0 );
            } else if ( '未滿3歲' === $value['member_identity_type'] ) { // 未滿3歲免費
                $totalFee += ( '參加' === $value['member_signup_activity'] ? 100 : 0 );
            }
        }
    }

    return $totalFee;
}

function genMailContent_reservation( $data ) {
    $column_basic = [
        'id' => '訂房序號',
        'name' => '姓名',
        'id_number' => '身分證字號',
        'phone' => '聯絡電話',
        'email' => 'E-Mail',
        'company' => '服務單位',
        'job_title' => '身分職稱',
        'receipt_title' => '收據抬頭',
        'EIN' => '統編',
        'signup' => '參加第十七屆離島研討會',
        'hotel' => '飯店名稱',
        'checkIn' => '住宿日期',
        'hotel_room' => '房型',
        'roomA' => '間數',
        'roomB' => '間數',
        'roomC' => '間數',
        'roomD' => '間數',
        'roomE' => '間數',
        'remark1' => '(喜來登) 備註',
        'remark2' => '(昇恆昌) 備註',
    ];

    $data_tmp = [];
    foreach ($data as $key => $value) {
        switch ($column_basic[$key]) {
          case '房型':
            $data_tmp[ $column_basic[$key] ] = ($value == 'A' ? '豪華雙床客房' : ($value == 'B' ? '市景豪華雙床客房' : ($value == 'C' ? '港景豪華雙床客房' : ($value == 'D' ? '港景豪華雙人客房' : '園景探索客房2人1室'))));
            break;

          default:
            $data_tmp[ $column_basic[$key] ] = $value;
            break;
        }
    }
    $data = $data_tmp;
    $data = array_filter($data, function($v){
        return ( '-' !== $v );
    });

    $html_data = '';
    foreach ($data as $key => $value) {
        $html_data .= "<tr>"
            . "<td style=\"border: 1px solid #337ab7; padding:2px 5px; text-align:center;\">{$key}</td>"
            . "<td style=\"border: 1px solid #337ab7; padding:2px 5px;\">{$value}</td>"
            . "</tr>\n";
    }

    ob_start();
?>
<p>感謝您報名第17屆離島資訊與技術研討會的優惠飯店的預訂，以下是您的訂房資料副本。</p>
<p>大會將儘速確認您的訂房資格是否符合飯店給予大會參加人員條件，確認無誤後，大會將再發送確認MAIL(預訂成功)。</p>
<p>確認預訂成功後，後續繳費方式將由飯店人員主動聯繫您。請多留意您的Email，以免遺漏重要訊息。</p>
<p>再次感謝您對於第17屆離島資訊技術與應用研討會的支持！</p>
<table style="border-collapse:collapse;">
<tbody>
<?= $html_data; ?>
</tbody>
</table>
<p>如報名資料無誤，繳費方式將由飯店人員主動聯繫您。請多留意您的Email，以免遺漏重要訊息。如有任何問題請mail: <a target="blank" href="mailto:wuhsinte@gms.npu.edu.tw">wuhsinte@gms.npu.edu.tw</a></p>
<p>再次感謝您報名參與第十七屆離島資訊技術與應用研討會。</p>
<?php
    $html_full = ob_get_contents();
    ob_clean();
    return $html_full;
}

function genMailContent_signup( $data ) {
    // $column_basic  = ['報名序號', '姓名', '性別', '身分證字號', '出生年月日', '服務單位', '身分職稱', '聯絡電話', 'E-Mail', '收據抬頭', '飲食習慣', '5月19日午餐', '參加5月19日晚宴', '大金門行程', '小金門行程', '是否註冊論文', '論文編號', '論文標題', '身分', '是否為科技部計畫論文', '科技部計畫編號', '計畫名稱', '計畫主持人', '計畫主持人任職單位', '備註', '認證碼', '隨行人員資料', '報名費'];
    // $column_member = ['姓名', '性別', '身分證字號', '出生年月日', '身分別', '聯絡電話', 'E-Mail', '飲食習慣', '5月19日午餐', '參加5月19日晚宴', '大金門行程', '小金門行程'];
    $column_basic = [
        'id' => '報名序號',
        'name' => '姓名',
        'gender' => '性別',
        'id_number' => '身分證字號',
        'birthday' => '出生年月日',
        'company' => '服務單位',
        'job_title' => '身分職稱',
        'phone' => '聯絡電話',
        'email' => 'E-Mail',
        'receipt_title' => '收據抬頭',
        'eating_habits' => '飲食習慣',
        'signup_food' => '加訂5/27午餐便當',
        // 'food_1' => '5月19日午餐',
        // 'food_2' => '參加5月19日晚宴',
        'signup_activity' => '七美行程',
        'signup_activity_1' => '選擇活動',
        'signup_activity_2' => '選擇交通工具',
        'reg_paper' => '是否註冊論文',
        'paper_id' => '論文編號',
        'paper_title' => '論文標題',
        // 'identity' => '身分',
        // 'is_most_plan' => '是否為科技部計畫論文',
        // 'most_plan_id' => '科技部計畫編號',
        // 'most_plan_name' => '計畫名稱',
        // 'most_plan_host' => '計畫主持人',
        // 'most_plan_su' => '計畫主持人任職單位',
        'comment' => '備註',
        'password' => '認證碼',
        'member_data' => '隨行人員資料',
        'fee_total' => '報名費',
    ];
    $column_member = [
        'member_name' => '姓名',
        'member_gender' => '性別',
        'member_id_number' => '身分證字號',
        'member_birthday' => '出生年月日',
        'member_identity_type' => '身分別',
        'member_phone' => '聯絡電話',
        'member_email' => 'E-Mail',
        'member_eating_habits' => '飲食習慣',
        'member_food_1' => '加訂5/25午餐',
        'member_food_2' => '加訂5/25喜來登晚宴',
        'member_food_3' => '加訂5/27午餐便當',
        'member_signup_activity' => '七美行程',
        'member_signup_activity_1' => '選擇活動',
        'member_signup_activity_2' => '選擇交通工具',
    ];


    $data['member_data'] = json_decode($data['member_data'], 1);
    if ( count($data['member_data']) ) {
        $text = '';
        foreach ($data['member_data'] as $key => $value) {
            $text .= "----------[隨行人員" . ( $key+1 ) . "]----------\n"
                . implode("\n", array_map(function($k, $v){ return "{$k}：{$v}"; }, $column_member, $value) ) . "\n";
        }
        $data['member_data'] = '<pre>' . substr($text, 0, -1) . '</pre>';
    } else {
        $data['member_data'] = '-';
    }

    unset($data['crc32']);

    // $data = array_combine($column_basic, $data);
    $data_tmp = [];
    foreach ($data as $key => $value) {
        $data_tmp[ $column_basic[$key] ] = $value;
    }
    $data = $data_tmp;
    $data = array_filter($data, function($v){
        return ( '-' !== $v );
    });

    $html_data = '';
    foreach ($data as $key => $value) {
        $html_data .= "<tr>"
            . "<td style=\"border: 1px solid #337ab7; padding:2px 5px; text-align:center;\">{$key}</td>"
            . "<td style=\"border: 1px solid #337ab7; padding:2px 5px;\">{$value}</td>"
            . "</tr>\n";
    }

    ob_start();
?>
<p>感謝您報名第十七屆離島資訊與技術研討會，以下是您的報名資料副本：</p>
<table style="border-collapse:collapse;">
<tbody>
<?= $html_data; ?>
</tbody>
</table>
<p>如報名資料無誤，以下是收款帳號：</p>
<blockquote style="display:inline-block; border-left:6px solid #337ab7; background-color:#eaf5fb; margin:5px 0; padding:6px 12px;">
    <span>立帳銀行：合作金庫銀行--高雄科技園區分行</span><br>
    <span>帳號：3524-717-001087</span><br>
    <span>戶名：台灣網路智能學會</span>
    <span>總行代號：006</span>
</blockquote>
<p>轉帳完成後，請至<a href="http://itaoi2018.npu.edu.tw/signup/payment-record.html">這裡</a>填寫繳費記錄，以利大會進行對帳。</p>
<p>再次感謝您報名參與第十七屆離島資訊技術與應用研討會。</p>
<?php
    $html_full = ob_get_contents();
    ob_clean();
    return $html_full;
}

function genMailContent_paymentRecord( $data ) {
    $column = ['報名序號', '繳費帳號或姓名', '繳費金額', '繳費時間'];
    $data = array_combine($column, (array)$data);

    $html_data = '';
    foreach ($data as $key => $value) {
        $html_data .= "<tr>"
            . "<td style=\"border: 1px solid #337ab7; padding:2px 5px; text-align:center;\">{$key}</td>"
            . "<td style=\"border: 1px solid #337ab7; padding:2px 5px;\">{$value}</td>"
            . "</tr>\n";
    }

    ob_start();
?>
<p>系統已收到您的繳費記錄，以下是您的繳費記錄副本：</p>
<table style="border-collapse:collapse;">
<tbody>
<?= $html_data; ?>
</tbody>
</table>
<p>再次感謝您報名參與第十七屆離島資訊技術與應用研討會。</p>
<?php
    $html_full = ob_get_contents();
    ob_clean();
    return $html_full;
}

function send_email($recipients, $subject, $body) {
    $ch = curl_init();
    ob_start();
?>
<hr>
<small>
    <p>※此為系統自動發信，請勿直接回覆，若有任何地方需要協助，請利用以下連絡方式與我們聯繫，謝謝您。</p>
    <blockquote style="display:inline-block; border-left:6px solid #337ab7; background-color:#eaf5fb; margin:5px 0; padding:6px 12px;">
        <span>電話：(06)9264115 ext. 3505 or 3502 吳信德助理教授</span><br>
        <span>E-Mail：<a href="mailto:wuhsinte@gms.npu.edu.tw">wuhsinte@gms.npu.edu.tw</a></span>
    </blockquote>
    <p>第十七屆離島資訊與技術研討會：<a target="_blank" href="http://itaoi2018.npu.edu.tw/">itaoi2018.npu.edu.tw</a></p>
</small>
<?php
    $body .= ob_get_contents();
    ob_clean();
    $ch = curl_init();
    $options = array(
        CURLOPT_HEADER         => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL            => EPS_HOST,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => http_build_query([
            'apikey'     => EPS_APIKEY,
            'channel'    => EPS_CHANNEL,
            'recipients' => $recipients,
            'subject'    => base64_encode($subject),
            'body'       => base64_encode($body),
        ]),
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    $result = json_decode($result);
    curl_close($ch);

    return ( 0 === (int)$result->status ? true : false );
}


function genSingupRecordTableHtml( $data ) {
    $column_basic  = ['報名序號', '姓名', '性別', '身分證字號', '出生年月日', '服務單位', '身分職稱', '聯絡電話', 'E-Mail', '收據抬頭', '飲食習慣', '加訂5/27午餐便當', '七美行程', '選擇活動', '選擇交通工具', '是否註冊論文', '論文編號', '論文標題', '隨行人員資料', '備註', '報名費', '繳費帳號或繳款者姓名', '繳費金額', '繳費時間'];
    $column_member = ['姓名', '性別', '身分證字號', '出生年月日', '身分別', '聯絡電話', 'E-Mail', '飲食習慣', '加訂5/25日午餐', '加訂5/25喜來登晚宴', '加訂5/27午餐便當', '七美行程', '選擇活動', '選擇交通工具'];

    $data['member_data'] = json_decode($data['member_data'], 1);
    if ( count($data['member_data']) ) {
        $text = '';
        foreach ($data['member_data'] as $key => $value) {
            $text .= "----------[隨行人員" . ( $key+1 ) . "]----------\n"
                . implode("\n", array_map(function($k, $v){ return "{$k}：{$v}"; }, $column_member, $value) ) . "\n";
        }
        // $data['member_data'] = '<pre>' . substr($text, 0, -1) . '</pre>';
        $data['member_data'] = substr($text, 0, -1);
    } else {
        $data['member_data'] = '-';
    }

    unset($data['crc32'], $data['password'], $data['confirmed']);
    $data = array_combine($column_basic, $data);
    $data = array_filter($data, function($v){
        return ( '-' !== $v );
    });

    $html_data = '<table class="table table-striped table-bordered"><tbody>';
    foreach ($data as $key => $value) {
        $html_data .= "<tr>"
            . "<td class=\"text-center\">{$key}</td>"
            . "<td>{$value}</td>"
            . "</tr>\n";
    }
    $html_data .= '</tbody></table>';
    return $html_data;
}
