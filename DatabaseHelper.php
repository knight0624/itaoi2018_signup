<?php
class_alias('DatabaseHelper', 'DH');
class DatabaseHelper {
    private $connection;
    private $host;
    private $dbname;
    private $username;
    private $password;
    private $transactionCount = 0;
    private $lastExecAffectCount;

    public function __construct($host, $dbname, $username, $password) {
        $this->host     = $host;
        $this->dbname   = $dbname;
        $this->username = $username;
        $this->password = $password;

        if ( false === $this->connect() ) {
            echo 'Connection error.';
            exit;
        }
    }

    public function connect() {
        if ( false == $this->connection ) {
            $this->connection = new PDO(
                'mysql:host=' . $this->host . ';dbname=' . $this->dbname . ';',
                $this->username, $this->password,
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                    PDO::ATTR_PERSISTENT         => true
                )
            );
        }

        return ( false != $this->connection );
    }

    public function beginTransaction() {
        if ( false === $this->inTransaction() ) {
            $this->connection->beginTransaction();
        }

        $this->transactionCount++;
    }

    public function inTransaction() {
        return $this->connection->inTransaction();
    }

    public function commit() {
        $this->transactionCount--;
        if ( 0 === $this->transactionCount ) {
            return $this->connection->commit();
        } elseif ( 0 > $this->transactionCount ) {
            throw new Exception('The database connection not in transaction.', 0);
            return false;
        } else {
            return true;
        }
    }

    public function rollback() {
        $this->transactionCount--;
        if ( 0 === $this->transactionCount ) {
            return $this->connection->rollback();
        } else {
            return true;
        }
    }

    public function rowCount() {
        return $this->lastExecAffectCount;
    }

    public function getTransactionCount() {
        return $this->transactionCount;
    }

    /**
     * @param string $sql
     * @param array  $parameters
     *
     * @return stdClass
     */
    public function sql_q( $sql, $parameters = [] ) {
        $sth = $this->connection->prepare($sql);
        $sth->execute($parameters);
        if( $sth->errorCode() != '00000' ){
            $error = $sth->errorInfo();
            throw new Exception('SQL execute failed. (Error #' . $error[1] .': ' . $error[2] . ')', $error[1]);
            return false;
        }
        $sth->setFetchMode(PDO::FETCH_OBJ);
        // $result = filter_var_array($sth->fetchAll(), FILTER_SANITIZE_MAGIC_QUOTES);
        // if( count($result) == 0 )
        //     $result = array();
        $result = $sth->fetchAll();
        return $result;
    }


    /**
     * @param string $sql
     * @param array  $parameters
     *
     * @return boolean
     */
    public function sql_e( $sql, $parameters ) {
        $sth = $this->connection->prepare($sql);
        $sth->execute($parameters);
        $this->lastExecAffectCount = $sth->rowCount();
        if( $sth->errorCode() != '00000' ){
            $error = $sth->errorInfo();
            throw new Exception('SQL execute failed. (Error #' . $error[1] .': ' . $error[2] . ')', $error[1]);
            $this->connection->rollback();
            return false;
        }
        return true;
    }

    /**
     * @return integer (If current connection not insert any data rows then return 0.)
     */
    public function lastInsertId() {
        return $this->connection->lastInsertId();
    }

    /**
     * @param string $arg_schemaDefinition (Both support table name and join syntax.)
     * @param array  $arg_targetColumn = []
     * @param array  $arg_parameters = []
     * @param array  $other_options (Limit, offset, order ... etc)
     * @return stdClass
     * @since 1.00-Beta
     * @version 2.02-Beta
     */
    public function select( $arg_schemaDefinition, array $arg_targetColumn = [], array $arg_parameters = [], array $other_options = [] ) {
        $sql              = '';
        $parameters       = [];
        $schemaDefinition = '';
        $syntax_where     = '';
        $syntax_groupBy   = '';
        $syntax_order     = '';
        $syntax_range     = '';

        if ( is_string($arg_schemaDefinition) ) {
            $schemaDefinition = (
                $this->haveJoinSyntax($arg_schemaDefinition) ?
                $arg_schemaDefinition:
                '`' . $arg_schemaDefinition . '`'
            );
        } else {
            throw new Exception("Incorrect schema definition.", 210);
            return false;
        }

        if ( 0 < count($arg_targetColumn) ) {
            $targetColumn = $this->implode($arg_targetColumn);
        } else {
            $targetColumn = '*';
        }

        if ( 0 < count($arg_parameters) ) {
            foreach ($arg_parameters as $key => $value) {
                $quote = ( false === strpos($key, '`') ? '`' : '' );
                if ( is_array($value) ) {
                    $bind = substr( str_repeat('?, ', count($value)), 0, -2 );
                    $syntax_where .= $quote . $key . $quote . ' IN (' . $bind . ') AND ';
                    $parameters = array_merge($parameters, $value);
                } else {
                    $syntax_where .= $quote . $key . $quote . ' = ? AND ';
                    $parameters[] = $value;
                }
            }
            $syntax_where = ' WHERE ' . substr($syntax_where, 0, -4);
        }

        if ( isset($other_options['groupby']) ) {
            foreach ($other_options['groupby'] as $value) {
                $quote = ( false === strpos($value, '`') ? '`' : '' );
                $syntax_groupBy .= $quote . $value . $quote . ', ';
            }
            $syntax_groupBy = ' GROUP BY ' . substr($syntax_groupBy, 0, -2);
        }

        if ( isset($other_options['order']) ) {
            $syntax_order = ' ORDER BY ';
            foreach ($other_options['order'] as $column => $sortType) {
                $quote = ( false === strpos($column, '`') ? '`' : '' );
                $syntax_order .= $quote . $column . $quote . $sortType . ', ';
            }
            $syntax_order = substr($syntax_order, 0, -2) . ' ';
        }

        if ( isset($other_options['rows']) && is_int($other_options['rows']) ) {
            $syntax_range = 'LIMIT ' . abs($other_options['rows']);
            if ( isset($other_options['paged']) && is_int($other_options['paged']) ) {
                $syntax_range .= ' OFFSET ' . abs($$other_options['paged']);
            } elseif ( isset($other_options['offset']) && is_int($other_options['offset']) ) {
                $syntax_range .= ' OFFSET ' . abs($$other_options['offset']);
            }
        }

        $sql = 'SELECT ' . $targetColumn . ' FROM ' . $schemaDefinition . $syntax_where . $syntax_groupBy . $syntax_order . $syntax_range;
        return $this->sql_q($sql, $parameters);
    }

    /**
     * @param string $arg_schemaDefinition (Only support table name.)
     * @param array  $arg_data
     *
     * @return boolean
     */
    public function insert( $arg_schemaDefinition, array $arg_data ) {
        $sql              = '';
        $parameters       = [];
        $schemaDefinition = '';
        $joinSyntax       = '';
        $syntax_column    = '';
        $syntax_values    = '';

        if ( is_string($arg_schemaDefinition) ) {
            if ( true == $this->haveJoinSyntax($arg_schemaDefinition) ) {
                $schemaDefinition = substr($arg_schemaDefinition, 0, strpos($arg_schemaDefinition, ' '));
                $joinSyntax       = substr($arg_schemaDefinition, strpos($arg_schemaDefinition, ' '));
            } else {
                $schemaDefinition = '`' . $arg_schemaDefinition . '`';
            }
        } else {
            throw new Exception("Incorrect schema definition.", 210);
            return false;
        }

        if ( 0 < count($arg_data) ) {
            foreach ($arg_data as $key => $value) {
                $syntax_column .= '`' . $key . '`, ';
                $parameters[] = $value;
            }
            $syntax_column = '(' . substr($syntax_column, 0, -2 ) . ')';
            $syntax_values = '(' . substr( str_repeat('?, ', count($parameters)), 0, -2 ) . ')';
        } else {
            throw new Exception("Data not found, please check value assignment.", 220);
            return false;
        }

        $sql = 'INSERT INTO ' . $schemaDefinition . $syntax_column . ' VALUES' . $syntax_values;
        return $this->sql_e($sql, $parameters);
    }


    /**
     * @param string $arg_schemaDefinition (Both support table name and join syntax.)
     * @param array  $arg_data
     * @param array  $arg_parameters = []
     *
     * @return boolean
     */
    public function update( $arg_schemaDefinition, array $arg_data, array $arg_parameters = [] ) {
        $sql              = '';
        $parameters       = [];
        $schemaDefinition = '';
        $syntax_set       = '';
        $syntax_where     = '';

        if ( is_string($arg_schemaDefinition) ) {
            $schemaDefinition = (
                $this->haveJoinSyntax($arg_schemaDefinition) ?
                $arg_schemaDefinition :
                '`' . $arg_schemaDefinition . '`'
            );
        } else {
            throw new Exception("Incorrect schema definition.", 210);
            return false;
        }

        if ( 0 < count($arg_data) ) {
            foreach ($arg_data as $key => $value) {
                $syntax_set .= '`' . $key . '` = ?, ';
                $parameters[] = $value;
            }
            $syntax_set = substr($syntax_set, 0, -2);
        } else {
            throw new Exception("Data not found, please check value assignment.", 220);
            return false;
        }

        if ( 0 < count($arg_parameters) ) {
            foreach ($arg_parameters as $key => $value) {
                $quote = ( false === strpos($key, '`') ? '`' : '' );
                if ( is_array($value) ) {
                    $bind = substr( str_repeat('?, ', count($value)), 0, -2 );
                    $syntax_where .= $quote . $key . $quote . ' IN (' . $bind . ') AND ';
                    $parameters = array_merge($parameters, $value);
                } else {
                    $syntax_where .= $quote . $key . $quote . ' = ? AND ';
                    $parameters[] = $value;
                }
            }
            $syntax_where = ' WHERE ' . substr($syntax_where, 0, -4);
        }

        $sql = 'UPDATE ' . $schemaDefinition . ' SET ' . $syntax_set . $syntax_where;
        return $this->sql_e($sql, $parameters);
    }

    /**
     * @param string $arg_schemaDefinition (Only support table name.)
     * @param array  $arg_parameters = []
     *
     * @return boolean
     */
    public function delete( $arg_schemaDefinition, array $arg_parameters = [] ) {
        $sql              = '';
        $parameters       = [];
        $schemaDefinition = '';
        $syntax_where     = '';

        if ( is_string($arg_schemaDefinition) ) {
            $schemaDefinition = (
                $this->haveJoinSyntax($arg_schemaDefinition) ?
                $arg_schemaDefinition :
                '`' . $arg_schemaDefinition . '`'
            );
        } else {
            throw new Exception("Incorrect schema definition.", 210);
            return false;
        }

        if ( 0 < count($arg_parameters) ) {
            foreach ($arg_parameters as $key => $value) {
                $syntax_where .= '`' . $key . '` = ? AND ';
                $parameters[] = $value;
            }
            $syntax_where = ' WHERE ' . substr($syntax_where, 0, -4);
        }

        $sql = 'DELETE FROM ' . $schemaDefinition . $syntax_where;
        return $this->sql_e($sql, $parameters);
    }

    /**
     * @param string $sql
     *
     * @return boolean
     */
    private function haveJoinSyntax( $sql ) {
        return ( false !== strpos($sql, 'JOIN') );
    }

    private function implode( $array ) {
        $rebuild = '';
        $targetColumn = implode($array, ', ');
        foreach ($array as $value) {
            if ( false === strpos($value, '`') ) {
                $rebuild .= '`' . $value . '`, ';
            } else {
                $rebuild .= $value . ', ';
            }
        }
        return substr($rebuild, 0, -2);
    }
}