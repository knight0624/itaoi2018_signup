<?php
    require_once('../config.php');
    require_once('../DatabaseHelper.php');

    $is_checkedPage = $_GET['checked'];
    $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
    $exec_result = null;
    if ( true === isset($_POST['submitOk']) ) {
        $exec_result = $db->update( 'hotel', ['confirmed' => 2], ['id' => $_POST['rid']] );
        $mail_data   = $db->select( 'hotel' , ['email'] , ['id' => $_POST['rid']]);
        for ($i=0; $i < count($mail_data); $i++) {
            send_email($mail_data[$i]->{'email'}, 'ITAOI 2018 訂房確認通知', sendMail_Ok() );
        }
    } else if (true === isset($_POST['submitClose'])) {
        $exec_result = $db->update( 'hotel', ['confirmed' => 0], ['id' => $_POST['rid']] );
        $mail_data   = $db->select( 'hotel' , ['email'] , ['id' => $_POST['rid']]);
        for ($i=0; $i < count($mail_data); $i++) {
            send_email($mail_data[$i]->{'email'}, 'ITAOI 2018 訂房取消通知', sendMail_Close() );
        }
    } else if (true === isset($_POST['submitCheck'])) {
        $exec_result = $db->update( 'hotel', ['confirmed' => 1], ['id' => $_POST['rid']] );
    }

    if ( false === isset($_GET['sort']) ) {
        $_GET['sort'] = 'id';
    }

    if ( false === isset($_GET['display_mode']) ) {
        $_GET['display_mode'] = 'full';
    }

    $data = $db->select( 'hotel', [], ['confirmed' => (int)$is_checkedPage], ['order' => [$_GET['sort'] => 'ASC']] );

    function sendMail_Ok() { //已確認
        ob_start();
?>
    <p>您的訂房資格符合飯店給予大會參加人員條件，</p>
    <p>已確認您的預定優惠飯店資訊，後續繳費方式將由飯店人員主動聯繫您。</p>
    <p>再次感謝您對於第 17 屆離島資訊技術與應用研討會的支持！</p>
    <p>如有任何問題請 mail: <a target="blank" href="mailto:wuhsinte@gms.npu.edu.tw">wuhsinte@gms.npu.edu.tw</a></p>
<?php
        $html_full = ob_get_contents();
        ob_clean();
        return $html_full;
    }

    function sendMail_Close() { //已取消
        ob_start();
?>
    <p>您的訂房資格不符合飯店給予大會參加人員條件，已取消您的預定優惠飯店資訊。</p>
    <p>再次感謝您對於第 17 屆離島資訊技術與應用研討會的支持！</p>
    <p>如有任何問題請 mail: <a target="blank" href="mailto:wuhsinte@gms.npu.edu.tw">wuhsinte@gms.npu.edu.tw</a></p>
<?php
        $html_full = ob_get_contents();
        ob_clean();
        return $html_full;
    }

    function send_email($recipients, $subject, $body) {
        $ch = curl_init();
        ob_start();
?>
    <hr>
    <small>
        <p>※此為系統自動發信，請勿直接回覆，若有任何地方需要協助，請利用以下連絡方式與我們聯繫，謝謝您。</p>
        <blockquote style="display:inline-block; border-left:6px solid #337ab7; background-color:#eaf5fb; margin:5px 0; padding:6px 12px;">
            <span>電話：(06)9264115 ext. 3505 or 3502 吳信德助理教授</span><br>
            <span>E-Mail：<a href="mailto:wuhsinte@gms.npu.edu.tw">wuhsinte@gms.npu.edu.tw</a></span>
        </blockquote>
        <p>第十七屆離島資訊與技術研討會：<a target="_blank" href="http://itaoi2018.npu.edu.tw/">itaoi2018.npu.edu.tw</a></p>
    </small>
<?php
        $body .= ob_get_contents();
        ob_clean();
        $ch = curl_init();
        $options = array(
            CURLOPT_HEADER         => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => EPS_HOST,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => http_build_query([
                'apikey'     => EPS_APIKEY,
                'channel'    => EPS_CHANNEL,
                'recipients' => $recipients,
                'subject'    => base64_encode($subject),
                'body'       => base64_encode($body),
            ]),
        );
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);

        return ( 0 === (int)$result->status ? true : false );
    }
?>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>報名系統 | ITAOI 2018 第十七屆離島資訊技術與應用研討會</title>
    <link rel="Shortcut Icon" type="image/x-icon" href="http://itaoi2018.npu.edu.tw/wp-content/uploads/2017/10/17屆logo_1.png" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css.map">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap-theme.min.css.map">
    <link rel="stylesheet" href="../jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="../jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../style.default.css">

    <style type="text/css">
        @media (min-height: 550px) { .registers-table-block { max-height:330px; } } /* 720 */
        @media (min-height: 598px) { .registers-table-block { max-height:378px; } } /* 768 */
        @media (min-height: 630px) { .registers-table-block { max-height:410px; } } /* 800 */
        @media (min-height: 730px) { .registers-table-block { max-height:510px; } } /* 900 */
        @media (min-height: 854px) { .registers-table-block { max-height:634px; } } /* 1024 */
        @media (min-height: 910px) { .registers-table-block { max-height:690px; } } /* 1080 */

        body {
            overflow: scroll;
        }

        .registers-table-block table th {
            white-space: nowrap;
            text-align: center;
        }

        /*.registers-table-block table .form-checkbox-checked {*/
        .registers-table-block table .form-checkbox-checked {
            background-color: rgba(255, 240, 193, 0.5);
        }

        .white-space-pre {
            white-space: pre-wrap;
            /*word-break: break-word;*/
            width: 30em;
        }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-xs-2">
            <a class="pull-left" href="http://itaoi2018.npu.edu.tw/">
                <img height="80" src="http://itaoi2018.npu.edu.tw/wp-content/uploads/2017/10/17屆logo_1.png" class="custom-logo" alt="" itemprop="logo">
            </a>
        </div>
        <div class="col-xs-10">
            <nav id="nav-bar" class="pull-left">
                <div class="container" style="margin-top: 10px;">
                    <div>
                        <ul class="nav nav-pills">
                            <li<?= ( $is_checkedPage == 0 ? ' class="active"' : '' ); ?>><a href="book_registers.php?checked=0">已取消預定列表</a></li>
                            <li<?= ( $is_checkedPage == 1 ? ' class="active"' : '' ); ?>><a href="book_registers.php?checked=1">未確認預定列表</a></li>
                            <li<?= ( $is_checkedPage == 2 ? ' class="active"' : '' ); ?>><a href="book_registers.php?checked=2">已確認預定列表</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <hr>
    <?php if ( null !== $exec_result ): ?>
        <?php if ( false == $exec_result ): ?>
            <div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">&times;</a>操作失敗，請稍候再嘗試；若持續出現此狀況，請聯絡網站管理者。</div>
        <?php else: ?>
            <div class="alert alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>操作成功。</div>
        <?php endif; ?>
    <?php endif; ?>
    <h2><?=($is_checkedPage == 0 ? '已取消預定列表' : ($is_checkedPage == 1 ? '未確認預定列表' : '已確認預定列表'));?></h2>
    <?php if ( 0 < count($data) ): ?>
    <form id="reg-form" method="post">
        <span class="func_selectAll">(
            <span class="func_sa_control hyper-link">全選</span>/
            <span class="func_sa_control hyper-link">全不選</span>
        )</span>
        <div class="pull-right">
            <?php
                if ($is_checkedPage == 0) {
             ?>
                    <button type="submit" name="submitCheck" class="btn btn-primary" style="margin-top:-36px;">將設為未確認</button>
            <?php
                } else if ($is_checkedPage == 1) {
             ?>
                    <button type="submit" name="submitOk" class="btn btn-primary" style="margin-top:-36px;">將設為已確認</button>
                    <button type="submit" name="submitClose" class="btn btn-danger" style="margin-top:-36px;">將設為已取消</button>
            <?php
                } else {
             ?>
                    <button type="submit" name="submitCheck" class="btn btn-primary" style="margin-top:-36px;">將設為未確認</button>
                    <a class="btn btn-success" href="book_excel.php?confirmed=<?= (int)$is_checkedPage; ?>" style="margin-top:-36px;">匯出成Excel檔</a>
            <?php
                }
             ?>
        </div>
        <div class="registers-table-block">
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                        <th>#</th>
                        <th>訂房序號</th>
                        <!-- <th>補發信件</th> -->
                        <th style="min-width:4em;">姓名</th>
                        <th>身分證字號</th>
                        <th>聯絡電話</th>
                        <th>E-mail</th>
                        <th>服務單位</th>
                        <th>身份職稱</th>
                        <th>收據抬頭</th>
                        <th>統編</th>
                        <th>參加第十七屆離島研討會</th>
                        <th>飯店名稱</th>
                        <th>住宿日期</th>
                        <th>房型</th>
                        <th>間數</th>
                        <th>備註</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $key => $value): ?>
                    <?php $room_num = "room".$value->hotel_room; ?>
                    <tr>
                        <td><input type="checkbox" name="rid[]" value="<?= htmlentities($value->id); ?>"></td>
                        <td><?= htmlentities($value->id);            ?></td>
                        <!-- <td><button class="btn btn-warning"><span class="glyphicon glyphicon-envelope"></span> Send</button></td> -->
                        <td><?= htmlentities($value->name);          ?></td>
                        <td><?= htmlentities($value->id_number);     ?></td>
                        <td><?= htmlentities($value->phone);         ?></td>
                        <td><?= htmlentities($value->email);         ?></td>
                        <td><?= htmlentities($value->company);       ?></td>
                        <td><?= htmlentities($value->job_title);     ?></td>
                        <td><?= htmlentities($value->receipt_title); ?></td>
                        <td><?= htmlentities($value->EIN);           ?></td>
                        <td><?= htmlentities($value->signup);        ?></td>
                        <td><?= htmlentities($value->hotel   );      ?></td>
                        <td><?= htmlentities($value->checkIn);       ?></td>
                        <td><?= htmlentities($value->hotel_room == 'A' ? '豪華雙床客房' : ($value->hotel_room == 'B' ? '市景豪華雙床客房' : ($value->hotel_room == 'C' ? '港景豪華雙床客房' : ($value->hotel_room == 'D' ? '港景豪華雙人客房' : '園景探索客房2人1室'))) ); ?></td>
                        <td><?= htmlentities($value->$room_num);     ?></td>
                        <?php if ($value->remark1 != ''): ?>
                            <td><?= htmlentities($value->remark1);       ?></td>
                        <?php else: ?>
                            <td><?= htmlentities($value->remark2);       ?></td>
                        <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div><!-- .registers-table-block -->
    <?php else: ?>
        <div>
            <p>查無資料。</p>
        </div>
    </form><!-- #reg-form -->
    <?php endif; ?>

    <div class="dialog_content" style="display:none;"></div>

    <!-- Latest compiled and minified JavaScript -->
    <script src="../jquery-3.1.1.min.js"></script>
    <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="../jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

    <script>
        function detectBrowser() {
            var browser=navigator.appName
            var b_version=navigator.appVersion
            var version=parseFloat(b_version)

            if ( browser == "Microsoft Internet Explorer" && ( (version <= 4) || (b_version.match('MSIE 9')) ) ) {
                alert('您的IE瀏覽器版本低於10.0，瀏覽本站將可能出現頁面顯示異常問題！請使用立即更新您的瀏覽器，或使用Google Chrome、mozilla Firefox瀏覽本站！');
            }
            console.log('Browser Name: ' + browser);
            console.log('Browser b_version: ' + b_version);
            console.log('Browser version: ' + version);
        }

        function clear_form( formDomAddr ) {
            $(formDomAddr+" :input").each(function(){
                switch($(this).attr('type')){
                    case 'radio':
                      $(this).attr("checked", false);
                    case 'checkbox':
                      $(this).attr("checked", false);
                    break;
                    case 'select-one':
                      $(this)[0].selectedIndex = 0;
                    break;
                    case 'text':
                      $(this).attr("value", "");
                    break;
                    case 'password':
                      $(this).attr("value", "");
                    //case 'hidden':
                    case 'textarea':
                      $(this).attr("value", "");
                    break;
                }
            });
        }

        function dialog_familyData( content ) {
            $('.dialog_content').html('<pre>' + content + '</pre>').dialog({
                title: '外加人員資料',
                minWidth: 400,
                modal: true,
            });
        }

        $(document).ready(function(){
            $('.func_sa_control').click(function(){
                var action = $(this).html()=='全選';
                $('#reg-form input[type="checkbox"]').each(function(){
                    if ( true == action ) {
                        $(this).prop('checked', 1).parent().parent().addClass('form-checkbox-checked');
                    } else {
                        $(this).prop('checked', 0).parent().parent().removeClass('form-checkbox-checked');
                    }
                });
            });

            $('#reg-form input[type="checkbox"]').on('change', function(){
                if ( true == $(this).is(':checked') ) {
                    $(this).parent().parent().addClass('form-checkbox-checked');
                } else {
                    $(this).parent().parent().removeClass('form-checkbox-checked');
                }
            });

            $('form').submit(function(){
                var checked_sum = 0;
                $(this).find('input[type="checkbox"]').each(function(){
                    if ( $(this).prop('checked') )
                        checked_sum++;
                });

                if ( checked_sum <= 0 ) {
                    alert('未選取任何項目.');
                    return false;
                }
            });

            clear_form('#reg-form');

            $('.dialog_family-data').click(function(){
                dialog_familyData($(this).attr('data-content'));
            });

            keypressing = '';
            $('body').on('keydown', function(e){
                keypressing = e.key;
            }).on('keyup', function(){
                keypressing = '';
            });

            $('form table > tbody > tr').on('click', function(){
                if ( 'Control' === keypressing ) {
                    var checkbox = $(this).children().first().children();
                    checkbox.prop('checked', !checkbox.prop('checked')).trigger('change');
                }
            });
        });
    </script>
</body>
</html>
