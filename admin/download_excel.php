<?php
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:filename=itaoi2018-signup-record.xls");

    require_once('../config.php');
    require_once('../DatabaseHelper.php');

    $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
    $data = $db->select( 'signup', [], ['confirmed' => 1] );

    $memberDataParser = function( $memberData ) {
        $column_member = ['姓名', '性別', '身分證字號', '出生年月日', '身分別', '聯絡電話', 'E-Mail', '飲食習慣', '加訂5/25午餐', '加訂5/25喜來登晚宴', '加訂5/27午餐便當', '七美行程', '選擇活動', '選擇交通工具'];
        $memberData = json_decode($memberData, 1);
        if ( count($memberData) ) {
            $text = '';
            foreach ($memberData as $key => $value) {
                $text .= "----------[隨行人員" . ( $key+1 ) . "]----------\n"
                    . implode("\n", array_map(function($k, $v){ return "{$k}：{$v}"; }, $column_member, $value) ) . "\n";
            }
            $memberData = substr($text, 0, -1);
        } else {
            $memberData = '-';
        }
        return $memberData;
    };
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>
<body>
    <table border="1">
        <thead>
            <tr>
                <th>#</th>
                <th>姓名</th>
                <th>性別</th>
                <th>身分證字號</th>
                <th>出生年月日</th>
                <th>服務單位</th>
                <th>身分職稱</th>
                <th>聯絡電話</th>
                <th>E-Mail</th>
                <th>收據抬頭</th>
                <th>飲食習慣</th>
                <th>加訂5/27午餐便當</th>
                <th>七美行程</th>
                <th>選擇活動</th>
                <th>選擇交通工具</th>
                <th>是否註冊論文</th>
                <th>論文編號</th>
                <th>論文標題</th>
                <th>隨行人員資料</th>
                <th>備註</th>
                <th>報名費</th>
                <th>繳費帳號或繳款者姓名</th>
                <th>繳費金額</th>
                <th>繳費時間</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $key => $value): ?>
            <tr>
                <td><?= htmlentities($value->id);                ?></td>
                <td><?= htmlentities($value->name);              ?></td>
                <td><?= htmlentities($value->gender);            ?></td>
                <td><?= htmlentities($value->id_number);         ?></td>
                <td><?= htmlentities($value->birthday);          ?></td>
                <td><?= htmlentities($value->company);           ?></td>
                <td><?= htmlentities($value->job_title);         ?></td>
                <td><?= htmlentities($value->phone);             ?></td>
                <td><?= htmlentities($value->email);             ?></td>
                <td><?= htmlentities($value->receipt_title);     ?></td>
                <td><?= htmlentities($value->eating_habits);     ?></td>
                <td><?= htmlentities($value->signup_food);       ?></td>
                <td><?= htmlentities($value->signup_activity);   ?></td>
                <td><?= htmlentities($value->signup_activity_1); ?></td>
                <td><?= htmlentities($value->signup_activity_2); ?></td>
                <td><?= htmlentities($value->reg_paper);         ?></td>
                <td><?= htmlentities($value->paper_id);          ?></td>
                <td><?= htmlentities($value->paper_title);       ?></td>
                <td><?= htmlentities( $memberDataParser( $value->member_data ) ); ?></td>
                <td><?= htmlentities($value->comment);           ?></td>
                <td><?= htmlentities($value->fee_total);         ?></td>
                <td><?= htmlentities($value->payment_account);   ?></td>
                <td><?= htmlentities($value->payment_fee);       ?></td>
                <td><?= htmlentities($value->payment_time);      ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>
