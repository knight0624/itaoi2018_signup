<?php
    require_once('../config.php');
    require_once('../DatabaseHelper.php');

    $is_checkedPage = (isset($_GET['checked']) && (1 == $_GET['checked']) );
    $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
    $exec_result = null;
    if ( true === isset($_POST['submit']) ) {
        $exec_result = $db->update( 'signup', ['confirmed' => ($is_checkedPage ? 0 : 1)], ['id' => $_POST['rid']] );
    }

    if ( false === isset($_GET['sort']) ) {
        $_GET['sort'] = 'id';
    }

    if ( false === isset($_GET['display_mode']) ) {
        $_GET['display_mode'] = 'full';
    }

    $data = $db->select( 'signup', [], ['confirmed' => (int)$is_checkedPage], ['order' => [$_GET['sort'] => 'ASC']] );
    $memberDataParser = function( $memberData ) {
        $column_member = ['姓名', '性別', '身分證字號', '出生年月日', '身分別', '聯絡電話', 'E-Mail', '飲食習慣', '加訂5/25午餐', '加訂5/25喜來登晚宴', '加訂5/27午餐便當', '七美行程', '選擇活動', '選擇交通工具'];
        $memberData = json_decode($memberData, 1);
        if ( count($memberData) ) {
            $text = '';
            foreach ($memberData as $key => $value) {
                $text .= "----------[隨行人員" . ( $key+1 ) . "]----------\n"
                    . implode("\n", array_map(function($k, $v){ return "{$k}：{$v}"; }, $column_member, $value) ) . "\n";
            }
            $memberData = substr($text, 0, -1);
        } else {
            $memberData = '-';
        }
        return $memberData;
    };
    // print_r($data);
    // exit;
?>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>報名系統 | ITAOI 2018 第十七屆離島資訊技術與應用研討會</title>
    <link rel="Shortcut Icon" type="image/x-icon" href="http://itaoi2018.npu.edu.tw/wp-content/uploads/2017/10/17屆logo_1.png" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css.map">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap-theme.min.css.map">
    <link rel="stylesheet" href="../jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="../jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../style.default.css">

    <style type="text/css">
        @media (min-height: 550px) { .registers-table-block { max-height:330px; } } /* 720 */
        @media (min-height: 598px) { .registers-table-block { max-height:378px; } } /* 768 */
        @media (min-height: 630px) { .registers-table-block { max-height:410px; } } /* 800 */
        @media (min-height: 730px) { .registers-table-block { max-height:510px; } } /* 900 */
        @media (min-height: 854px) { .registers-table-block { max-height:634px; } } /* 1024 */
        @media (min-height: 910px) { .registers-table-block { max-height:690px; } } /* 1080 */

        /*.registers-table-block {
            overflow: scroll;
        }*/
        body {
            overflow: scroll;
        }

        .registers-table-block table th {
            white-space: nowrap;
            text-align: center;
        }

        /*.registers-table-block table .form-checkbox-checked {*/
        .registers-table-block table .form-checkbox-checked {
            background-color: rgba(255, 240, 193, 0.5);
        }


        /*.container {
            width: auto;
            padding: 0 30px;
        }*/

        .white-space-pre {
            white-space: pre-wrap;
            /*word-break: break-word;*/
            width: 30em;
        }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-xs-12">
            <a class="pull-left" href="http://itaoi2018.nqu.edu.tw/">
                <img width="64" height="64" src="http://itaoi2018.nqu.edu.tw/wp-content/uploads/2016/10/logo_128x128.png" class="custom-logo" alt="" itemprop="logo">
            </a>
            <nav id="nav-bar" class="pull-left">
                <div class="container">
                    <div>
                        <ul class="nav nav-pills">
                            <li<?= ( $is_checkedPage ? '' : ' class="active"' ); ?>><a href="view_registers.php">未確認繳費列表</a></li>
                            <li<?= ( $is_checkedPage ? ' class="active"' : '' ); ?>><a href="view_registers.php?checked=1">已確認繳費列表</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <hr>
    <?php if ( null !== $exec_result ): ?>
        <?php if ( false == $exec_result ): ?>
            <div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">&times;</a>操作失敗，請稍候再嘗試；若持續出現此狀況，請聯絡網站管理者。</div>
        <?php else: ?>
            <div class="alert alert-success"><a class="close" data-dismiss="alert" href="#">&times;</a>操作成功。</div>
        <?php endif; ?>
    <?php endif; ?>
    <h2><?=($is_checkedPage ? '已確認繳費列表' : '未確認繳費列表');?></h2>
    <?php if ( 0 < count($data) ): ?>
    <form id="reg-form" method="post">
        <span class="func_selectAll">(
            <span class="func_sa_control hyper-link">全選</span>/
            <span class="func_sa_control hyper-link">全不選</span>
        )</span>
        <span>(
            排序方式：<?php $sort_type = $_GET['sort']; unset($_GET['sort']); ?>
            <span><a class="hyper-link" href="?<?php $_GET['sort'] = 'id'; echo http_build_query($_GET); ?>"<?=( $sort_type == 'id' ? ' style="color:#FF6600;"' : '' );?>>報名序號</a></span>
            <span>/</span>
            <span><a class="hyper-link" href="?<?php $_GET['sort'] = 'paper_id'; echo http_build_query($_GET); ?>"<?=( $sort_type == 'paper_id' ? ' style="color:#FF6600;"' : '' );?>>論文編號</a></span>
        )</span>
        <span>(
            顯示模式：<?php $display_mode = $_GET['display_mode']; unset($_GET['display_mode']); $_GET['sort'] = $sort_type; ?>
            <span><a class="hyper-link" href="?<?php $_GET['display_mode'] = 'full'; echo http_build_query($_GET); ?>"<?=( $display_mode == 'full' ? ' style="color:#FF6600;"' : '' );?>>完整</a></span>
            <span>/</span>
            <span><a class="hyper-link" href="?<?php $_GET['display_mode'] = 'brief'; echo http_build_query($_GET); ?>"<?=( $display_mode == 'brief' ? ' style="color:#FF6600;"' : '' );?>>精簡</a></span>
        )</span>
        <div class="pull-right">
            <button type="submit" name="submit" class="btn btn-primary" style="margin-top:-36px;">將勾選項目設為<?= ( $is_checkedPage ? '未' : '已'); ?>確認</button>
            <a class="btn btn-success" href="download_excel.php?confirmed=<?= (int)$is_checkedPage; ?>" style="margin-top:-36px;">匯出成Excel檔</a>
        </div>
    <?php if ( 'brief' == $display_mode ): ?>
        <div class="registers-table-block">
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                        <th>#</th>
                        <th>序號</th>
                        <th style="min-width:4em;">姓名</th>
                        <th>性別</th>
                        <th>服務單位/機關</th>
                        <th>身份職稱</th>
                        <th>聯絡電話</th>
                        <th>E-mail</th>
                        <th style="min-width:15em;">收據抬頭</th>
                        <th>繳費帳號或繳款者姓名</th>
                        <th>報名費</th>
                        <th>繳費金額</th>
                        <th>繳費時間</th>
                        <th style="min-width:20em;">備註</th>
                        <th>補發信件</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $key => $value): ?>
                    <tr>
                        <td><input type="checkbox" name="rid[]" value="<?= htmlentities($value->id); ?>"></td>
                        <td><?= htmlentities($value->id);              ?></td>
                        <td><?= htmlentities($value->name);            ?></td>
                        <td><?= htmlentities($value->gender);          ?></td>
                        <td><?= htmlentities($value->company);         ?></td>
                        <td><?= htmlentities($value->job_title);       ?></td>
                        <td><?= htmlentities($value->phone);           ?></td>
                        <td><?= htmlentities($value->email);           ?></td>
                        <td><?= htmlentities($value->receipt_title);   ?></td>
                        <td><?= htmlentities($value->payment_account); ?></td>
                        <td><?= htmlentities($value->fee_total);       ?></td>
                        <td><?= htmlentities($value->payment_fee);     ?></td>
                        <td><?= htmlentities($value->payment_time);    ?></td>
                        <td class="white-space-pre"><?= htmlentities($value->comment); ?></td>
                        <td><a target="_blank" class="btn btn-warning btn-sm" href="../api.php?action=re-send-mail-signup-record&id=<?= $value->id; ?>"><span class="glyphicon glyphicon-envelope"></span></a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div><!-- .registers-table-block -->
    <?php else: ?>
        <div class="registers-table-block">
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                        <th>#</th>
                        <th>#</th>
                        <th>姓名</th>
                        <th>性別</th>
                        <th>身分證字號</th>
                        <th>出生年月日</th>
                        <th style="min-width:15em;">服務單位</th>
                        <th style="min-width:6em;">身分職稱</th>
                        <th>聯絡電話</th>
                        <th>E-Mail</th>
                        <th style="min-width:15em;">收據抬頭</th>
                        <th>飲食習慣</th>
                        <th>加訂5/27午餐便當</th>
                        <th>七美行程</th>
                        <th>選擇活動</th>
                        <th>選擇交通工具</th>
                        <th>是否註冊論文</th>
                        <th style="min-width:12em;">論文編號</th>
                        <th style="min-width:20em;">論文標題</th>
                        <th style="min-width:20em;">隨行人員資料</th>
                        <th style="min-width:20em;">備註</th>
                        <th>報名費</th>
                        <th>繳費帳號或繳款者姓名</th>
                        <th>繳費金額</th>
                        <th style="min-width:10em;">繳費時間</th>
                        <th>補發信件</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $key => $value): ?>
                    <tr>
                        <td><input type="checkbox" name="rid[]" value="<?= htmlentities($value->id); ?>"></td>
                        <td><?= htmlentities($value->id);                ?></td>
                        <td><?= htmlentities($value->name);              ?></td>
                        <td><?= htmlentities($value->gender);            ?></td>
                        <td><?= htmlentities($value->id_number);         ?></td>
                        <td><?= htmlentities($value->birthday);          ?></td>
                        <td><?= htmlentities($value->company);           ?></td>
                        <td><?= htmlentities($value->job_title);         ?></td>
                        <td><?= htmlentities($value->phone);             ?></td>
                        <td><?= htmlentities($value->email);             ?></td>
                        <td><?= htmlentities($value->receipt_title);     ?></td>
                        <td><?= htmlentities($value->eating_habits);     ?></td>
                        <td><?= htmlentities($value->signup_food);       ?></td>
                        <td><?= htmlentities($value->signup_activity);   ?></td>
                        <td><?= htmlentities($value->signup_activity_1); ?></td>
                        <td><?= htmlentities($value->signup_activity_2); ?></td>
                        <td><?= htmlentities($value->reg_paper);         ?></td>
                        <td><?= htmlentities($value->paper_id);          ?></td>
                        <td><?= htmlentities($value->paper_title);       ?></td>
                        <td class="white-space-pre"><?= htmlentities( $memberDataParser( $value->member_data ) ); ?></td>
                        <td class="white-space-pre"><?= htmlentities($value->comment); ?></td>
                        <td><?= htmlentities($value->fee_total);         ?></td>
                        <td><?= htmlentities($value->payment_account);   ?></td>
                        <td><?= htmlentities($value->payment_fee);       ?></td>
                        <td><?= htmlentities($value->payment_time);      ?></td>
                        <td><a target="_blank" class="btn btn-warning btn-sm" href="../api.php?action=re-send-mail-signup-record&id=<?= $value->id; ?>"><span class="glyphicon glyphicon-envelope"></span></a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div><!-- .registers-table-block -->
    <?php endif; ?>
        <?php else: ?>
        <div>
            <p>查無資料。</p>
        </div>
    </form><!-- #reg-form -->
    <?php endif; ?>

    <div class="dialog_content" style="display:none;"></div>

    <!-- Latest compiled and minified JavaScript -->
    <script src="../jquery-3.1.1.min.js"></script>
    <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="../jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

    <script>
        function detectBrowser() {
            var browser=navigator.appName
            var b_version=navigator.appVersion
            var version=parseFloat(b_version)

            if ( browser == "Microsoft Internet Explorer" && ( (version <= 4) || (b_version.match('MSIE 9')) ) ) {
                alert('您的IE瀏覽器版本低於10.0，瀏覽本站將可能出現頁面顯示異常問題！請使用立即更新您的瀏覽器，或使用Google Chrome、mozilla Firefox瀏覽本站！');
            }
            console.log('Browser Name: ' + browser);
            console.log('Browser b_version: ' + b_version);
            console.log('Browser version: ' + version);
        }

        function clear_form( formDomAddr ) {
            $(formDomAddr+" :input").each(function(){
                switch($(this).attr('type')){
                    case 'radio':
                      $(this).attr("checked", false);
                    case 'checkbox':
                      $(this).attr("checked", false);
                    break;
                    case 'select-one':
                      $(this)[0].selectedIndex = 0;
                    break;
                    case 'text':
                      $(this).attr("value", "");
                    break;
                    case 'password':
                      $(this).attr("value", "");
                    //case 'hidden':
                    case 'textarea':
                      $(this).attr("value", "");
                    break;
                }
            });
        }

        function dialog_familyData( content ) {
            $('.dialog_content').html('<pre>' + content + '</pre>').dialog({
                title: '外加人員資料',
                minWidth: 400,
                modal: true,
            });
        }

        $(document).ready(function(){
            $('.func_sa_control').click(function(){
                var action = $(this).html()=='全選';
                $('#reg-form input[type="checkbox"]').each(function(){
                    if ( true == action ) {
                        $(this).prop('checked', 1).parent().parent().addClass('form-checkbox-checked');
                    } else {
                        $(this).prop('checked', 0).parent().parent().removeClass('form-checkbox-checked');
                    }
                });
            });

            $('#reg-form input[type="checkbox"]').on('change', function(){
                if ( true == $(this).is(':checked') ) {
                    $(this).parent().parent().addClass('form-checkbox-checked');
                } else {
                    $(this).parent().parent().removeClass('form-checkbox-checked');
                }
            });

            $('form').submit(function(){
                var checked_sum = 0;
                $(this).find('input[type="checkbox"]').each(function(){
                    if ( $(this).prop('checked') )
                        checked_sum++;
                });

                if ( checked_sum <= 0 ) {
                    alert('未選取任何項目.');
                    return false;
                }
            });

            clear_form('#reg-form');

            $('.dialog_family-data').click(function(){
                dialog_familyData($(this).attr('data-content'));
            });

            keypressing = '';
            $('body').on('keydown', function(e){
                keypressing = e.key;
            }).on('keyup', function(){
                keypressing = '';
            });

            $('form table > tbody > tr').on('click', function(){
                if ( 'Control' === keypressing ) {
                    var checkbox = $(this).children().first().children();
                    checkbox.prop('checked', !checkbox.prop('checked')).trigger('change');
                }
            });
        });
    </script>
</body>
</html>
