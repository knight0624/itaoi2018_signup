<?php
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:filename=itaoi2018-signup-record.xls");

    require_once('../config.php');
    require_once('../DatabaseHelper.php');

    $db = new DH(DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD);
    $data = $db->select( 'hotel', [], ['confirmed' => 2] );
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>
<body>
    <table border="1">
        <thead>
            <tr>
                <th>訂房序號</th>
                <th>姓名</th>
                <th>身分證字號</th>
                <th>聯絡電話</th>
                <th>E-mail</th>
                <th>服務單位</th>
                <th>身份職稱</th>
                <th>收據抬頭</th>
                <th>統編</th>
                <th>參加第十七屆離島研討會</th>
                <th>飯店名稱</th>
                <th>住宿日期</th>
                <th>房型</th>
                <th>間數</th>
                <th>備註</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $key => $value): ?>
            <?php $room_num = "room".$value->hotel_room; ?>
            <tr>
                <td><?= htmlentities($value->id);            ?></td>
                <td><?= htmlentities($value->name);          ?></td>
                <td><?= htmlentities($value->id_number);     ?></td>
                <td><?= htmlentities('\''.$value->phone);    ?></td>
                <td><?= htmlentities($value->email);         ?></td>
                <td><?= htmlentities($value->company);       ?></td>
                <td><?= htmlentities($value->job_title);     ?></td>
                <td><?= htmlentities($value->receipt_title); ?></td>
                <td><?= htmlentities($value->EIN);           ?></td>
                <td><?= htmlentities($value->signup);        ?></td>
                <td><?= htmlentities($value->hotel   );      ?></td>
                <td><?= htmlentities($value->checkIn);       ?></td>
                <td><?= htmlentities($value->hotel_room == 'A' ? '豪華雙床客房' : ($value->hotel_room == 'B' ? '市景豪華雙床客房' : ($value->hotel_room == 'C' ? '港景豪華雙床客房' : ($value->hotel_room == 'D' ? '港景豪華雙人客房' : '園景探索客房2人1室'))) ); ?></td>
                <td><?= htmlentities($value->$room_num);     ?></td>
                <?php if ($value->remark1 != ''): ?>
                    <td><?= htmlentities($value->remark1);       ?></td>
                <?php else: ?>
                    <td><?= htmlentities($value->remark2);       ?></td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>
